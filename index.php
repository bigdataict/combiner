<!DOCTYPE html>
<html>
<head>
<style>
.msg {
  padding: 15px;
  font-family: 'Arial';
  background: #eee;
  color: 333;
  margin-bottom: 15px;
}
.error {
  background: red;
  color: white;
}
.success {
  background: green;
  color: white;
}
</style>
</head>
<body>
  <?php
  include 'config.inc.php';

  $dsn = 'mysql:host=localhost;dbname=bigdata;charset=utf8';
  $username = 'root';
  $password = 'galaxy';


  try {
    $con = new PDO($dsn, $username, $password);
  }
  catch(PDOException $e) {
    die('Could not connect to the database:<br/>' . $e);
  }
  $con->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
  $con->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

  $stmt = $con->prepare("INSERT INTO tweets (
    metadata,
    created_at,
    contributors,
    id_str,
    coordinates,
    current_user_retweet,
    entities,
    favorite_count,
    favorited,
    in_reply_to_screen_name,
    filter_level,
    in_reply_to_status_id_str,
    lang,
    place,
    possibly_sensitive,
    quoted_status_id_str,
    quoted_status,
    scopes,
    retweet_count,
    retweeted,
    retweeted_status,
    source,
    text,
    truncated,
    user,
    withheld_copyright,
    withheld_in_countries,
    withheld_scope
  ) VALUES (
    :metadata,
    :created_at,
    :contributors,
    :id_str,
    :coordinates,
    :current_user_retweet,
    :entities,
    :favorite_count,
    :favorited,
    :in_reply_to_screen_name,
    :filter_level,
    :in_reply_to_status_id_str,
    :lang,
    :place,
    :possibly_sensitive,
    :quoted_status_id_str,
    :quoted_status,
    :scopes,
    :retweet_count,
    :retweeted,
    :retweeted_status,
    :source,
    :text,
    :truncated,
    :user,
    :withheld_copyright,
    :withheld_in_countries,
    :withheld_scope
  )");

  $properties = [
    'metadata',
    'created_at',
    'contributors',
    'id_str',
    'coordinates',
    'current_user_retweet',
    'entities',
    'favorite_count',
    'favorited',
    'in_reply_to_screen_name',
    'filter_level',
    'in_reply_to_status_id_str',
    'lang',
    'place',
    'possibly_sensitive',
    'quoted_status_id_str',
    'quoted_status',
    'scopes',
    'retweet_count',
    'retweeted',
    'retweeted_status',
    'source',
    'text',
    'truncated',
    'user',
    'withheld_copyright',
    'withheld_in_countries',
    'withheld_scope'
  ];

  $count = 0;
  $files = preg_grep('/^([^.])/', scandir('input/'));
  foreach($files as $filename) {
    $file = file_get_contents('input/' . $filename);
    $data = json_decode($file, true);

    foreach ($data as $tweet)
    {
      $tweet_id = json_encode($tweet['id_str']);
      $query = $con->query("SELECT id_str FROM tweets WHERE id_str = '$tweet_id'");
      $row = $query->fetchAll(PDO::FETCH_ASSOC);

      if (count($row) <= 0)
      {
        $event = [];
        foreach ($properties as $property)
        {
          if (isset($tweet[$property]))
          {
            array_push($event, json_encode($tweet[$property]));
          }
          else {
            array_push($event, null);
          }
        }

        $stmt->execute($event);

        $count++;
        echo '<div class="msg success">';
        echo 'Saved ' . $count;
        echo '</div>';
      }
      else {
        $count++;
        echo '<div class="msg error">';
        echo 'Duplicate ' . $count;
        echo '</div>';
      }
    }
  }


  ?>
</body>
