<?php

$dsn = 'mysql:host=localhost;dbname=<your db>;charset=utf8';
$username = '<your username>';
$password = '<your password>';


try {
  $con = new PDO($dsn, $username, $password);
}
catch(PDOException $e) {
  die('Could not connect to the database:<br/>' . $e);
}
$con->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
$con->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

?>
