<?php

include 'config.inc.php';

$query = $con->query("SELECT * FROM tweets");
$row = $query->fetchAll(PDO::FETCH_ASSOC);

$data = [];

foreach ($row as $tweet)
{
  $content = [];
  foreach ($tweet as $key => $value)
  {
    $content[$key] = json_decode($value);
  }

  array_push($data, $content);
}

$log = fopen('output/data.json', 'w');
fwrite($log, json_encode($data));
header('Content-Type: application/json');
echo json_encode($data);

?>
